#include <stdio.h>
#include <stdlib.h>

int main()
{
/* This application is intended to make determining cost of product/supplies purchased from Canature WaterGroup/Hydrotech Water much easier and fool proof.
    In order to use this program, you'll need know what percentage off of list price you're buying at. If you don't know this please contact your sales rep.*/
     printf("            $$    \n");
     printf("         $$$$$$$$        CCCCC   OOOOO   SSSSS   TTTTTT\n");
     printf("        $$$ $$           CC      OO OO    SS       TT\n");
     printf("         $$$$$           CC      OO OO      SS     TT\n");
     printf("          $$$$           CCCCC   OOOOO   SSSSS     TT\n");
     printf("           $$$\n");
     printf("            $$$          CCCCC    AA    LL      CCCCC\n");
     printf("            $$$$         CC     AA  AA  LL      CC\n");
     printf("            $$$$$        CC     AAAAAA  LL      CC\n");
     printf("            $$ $$$       CCCCC  AA  AA  LLLLL   CCCCC\n");
     printf("         $$$$$$$$    \n");
     printf("            $$\n");

     printf("                     Welcome to the Cost Calculator\n");
     printf("                              Version 1.0\n");
     /* printf("  TTTTTTTT   HH    HH   HH    HH    EEEEE   WW  WW  WW    AA   YY   YY\n");
     printf("     TT      HH    HH   HH    HH    EE       WW WW WW   AA  AA  YY  YY\n");
     printf("     TT      HHHHHHHH   HHHHHHHH    EEEEE     WWWWWW    AAAAAA   YYYYY\n");
     printf("     TT      HH    HH   HH    HH    EE        WW  WW    AA  AA      YY\n");
     printf("     TT      HH    HH   HH    HH    EEEEE     WW  WW    AA  AA   YYYYY\n");
     printf("\n");
     printf("                                                   OOO   FFFF  IIII  LL\n");
     printf("                                                  OO OO  FF     II   LL\n");
     printf("                                                  OO OO  FFFF   II   LL\n");
     printf("                                                   OOO   FF    IIII  LLLL\n");
     */
     printf(" ┌───────────────────────────────────────────────────────────────────────────┐\n");
     printf(" │      This application's source is available under The Unlicense           │\n");
     printf(" │         and is provided AS-IS with NO WARRANTIES of ANY KIND!             │\n");
     printf(" │  For more information, consult LICENSE.txt that accompanied this program  │\n");
     printf(" │                 Cost Calculator written by N.T. Crotser                   │\n");
     printf(" │                       ntc [at] crotsertech.co                             │\n");
     printf(" └───────────────────────────────────────────────────────────────────────────┘\n\n");

          double multiply;
     double listprice;
     double cost;

     char sentinel = ' '; // sentinel for while loop

     printf("Enter your cost multiplier:   ");
     scanf("%lf", &cost);

     while(sentinel != 'q') // Use while loop instead of goto
     {
       printf("Enter the list price:   ");
       scanf("%lf", &listprice);
       multiply = listprice * cost;

       /* printf("The Item's List Price is: %f\n", listprice); */

       printf("*Your Cost Is: %.2f*\n\n", multiply);  // Limit trailing zeros

       printf("Press 'q' to Quit otherwise → \n"); // Illusory choice, 'q' is the only recognized choice
       scanf("%c", &sentinel);
     }
}
