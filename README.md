THE COST CALCULATOR
The project source can be found here:
https://gitlab.com/ntcrotser/cost-calculator
This is an attempt to provide easy, foolproof calculations for cost vs. list price.


AUTHORS
-N. T. Crotser- ntc@crotsertech.co
-Dave Brown- dontcallmebrownie@gmail.com

LICENSE
All of the files in this project are under The Unlicense.  A copy of the
license is available in the LICENSE file.
